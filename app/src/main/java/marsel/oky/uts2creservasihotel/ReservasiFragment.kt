package marsel.oky.uts2creservasihotel

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_reservasi.*
import kotlinx.android.synthetic.main.fragment_reservasi.view.*
import kotlinx.android.synthetic.main.fragment_reservasi.view.sp_penyewa

class ReservasiFragment : Fragment(), View.OnClickListener {

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var AdapterJenis: SimpleCursorAdapter
    lateinit var AdapterPenyewa: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaJenis : String=""
    var arrJenis = ArrayList<String>()
    var arrPenyewa = ArrayList<String>()
    lateinit var db : SQLiteDatabase
    var namaPenyewa :String=""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_reservasi,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.sp_penyewa.onItemSelectedListener = PenyewaSelect
        v.sp_jenis.onItemSelectedListener = JenisSelect
        v.delete_res.setOnClickListener(this)
        v.add_res.setOnClickListener(this)
        v.edit_res.setOnClickListener(this)
        v.ls_res.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataPenyewa()
        showDataJenis()
        showDataReservasi()
    }

    val PenyewaSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sp_penyewa.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = AdapterPenyewa.getItem(position) as Cursor
            namaPenyewa = c.getString(c.getColumnIndex("_id"))
        }
    }

    val JenisSelect = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            sp_jenis.setSelection(0,true)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val c : Cursor = AdapterJenis.getItem(position) as Cursor
            namaJenis = c.getString(c.getColumnIndex("_id"))
        }
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        v.et_reservasi.setText(c.getString(c.getColumnIndex("_id")))
        namaJenis = c.getString(c.getColumnIndex("namaJenis"))
        namaPenyewa = c.getString(c.getColumnIndex("namaPenyewa"))
        v.et_cekin.setText(c.getString(c.getColumnIndex("cekin")))
        v.et_cekout.setText(c.getString(c.getColumnIndex("cekout")))
        v.et_lama.setText(c.getString(c.getColumnIndex("lama")))
        v.sp_jenis.setSelection(getIndex(v.sp_jenis,namaJenis,arrJenis))
        v.sp_penyewa.setSelection(getIndex(v.sp_penyewa,namaPenyewa,arrPenyewa))
    }

    fun getIndex(spinner: Spinner, myString: String, arrayL: ArrayList<String>): Int {
        var a = spinner.count
        Log.d("Output","Jumlah item dalam array = $a")
        var b : String = ""
        for (i in 0 until a) {
            b = arrayL.get(i)
            Log.d("Output","Index ke $i = $b")
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun showDataReservasi(){
        var sql = "select r.nores as _id, p.nama as namaPenyewa, j.nama_jns as namaJenis, r.cekin, r.cekout, r.lama from reservasi r, penyewa p , jenis j " +
                "where r.id_penyewa=p.id_penyewa and r.id_jns=j.id_jns "
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_reservasi,c,
            arrayOf("_id","namaPenyewa","namaJenis","cekin","cekout","lama"), intArrayOf(R.id.tx_reservasi,R.id.tx_sewa,R.id.tx_jeniskamar,R.id.tx_cekin,R.id.tx_cekout,R.id.tx_lama),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.ls_res.adapter = lsAdapter
    }

    fun showDataPenyewa(){
        val c : Cursor = db.rawQuery("select nama as _id from penyewa order by nama asc",null)
        AdapterPenyewa = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        AdapterPenyewa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.sp_penyewa.adapter = AdapterPenyewa
        v.sp_penyewa.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrPenyewa.add(temp) //add the item
            c.moveToNext()
        }
    }

    fun showDataJenis(){
        val c : Cursor = db.rawQuery("select nama_jns as _id from jenis order by nama_jns asc",null)
        AdapterJenis = SimpleCursorAdapter(thisParent,android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        AdapterJenis.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.sp_jenis.adapter = AdapterJenis
        v.sp_jenis.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrJenis.add(temp) //add the item
            c.moveToNext()
        }
    }

    fun insertDataReservasi(nores: String , id_penyewa : Int, id_jns: Int, cekin: String,cekout: String,lama: String){
        var sql = "insert into reservasi(nores, id_penyewa, id_jns,cekin,cekout,lama) values (?,?,?,?,?,?)"
        db.execSQL(sql, arrayOf(nores,id_penyewa,id_jns,cekin,cekout,lama))
        showDataReservasi()
    }
    fun updateDataReservasi(nores: String , id_penyewa : Int, id_jns: Int, cekin: String,cekout: String,lama: String){
        var cv : ContentValues = ContentValues()
        cv.put("id_penyewa",id_penyewa)
        cv.put("id_jns",id_jns)
        cv.put("cekin",cekin)
        cv.put("cekout",cekout)
        cv.put("lama",lama)
        db.update("reservasi",cv,"nores = '$nores'",null)
        showDataReservasi()
    }
    fun deleteDataNilai(nores: String){
        db.delete("reservasi","nores = '$nores'",null)
        showDataReservasi()
    }

    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        var sqlPenyewa = "select id_penyewa from penyewa where nama = '$namaPenyewa'"
        var sqlJenis = "select id_jns from jenis where nama_jns = '$namaJenis'"
        val c : Cursor = db.rawQuery(sqlPenyewa,null)
        val c1 : Cursor = db.rawQuery(sqlJenis,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            insertDataReservasi(
                v.et_reservasi.text.toString(),
                c.getInt(c.getColumnIndex("id_penyewa")),
                c1.getInt(c1.getColumnIndex("id_jns")),
                v.et_cekin.text.toString(),
                v.et_cekout.text.toString(),
                v.et_lama.text.toString())
            v.et_reservasi.setText("")
            v.sp_penyewa.setSelection(0)
            v.sp_jenis.setSelection(0)
            v.et_cekin.setText("")
            v.et_cekout.setText("")
            v.et_lama.setText("")
        }
    }

    val btnUpdateDialog = DialogInterface.OnClickListener { dialog, which ->
        var sqlPenyewa = "select id_penyewa from penyewa where nama = '$namaPenyewa'"
        var sqlJenis = "select id_jns from jenis where nama_jns = '$namaJenis'"
        val c : Cursor = db.rawQuery(sqlPenyewa,null)
        val c1 : Cursor = db.rawQuery(sqlJenis,null)
        if(c.count>0 && c1.count>0){
            c.moveToFirst()
            c1.moveToFirst()
            updateDataReservasi(
                v.et_reservasi.text.toString(),
                c.getInt(c.getColumnIndex("id_penyewa")),
                c1.getInt(c1.getColumnIndex("id_jns")),
                v.et_cekin.text.toString(),
                v.et_cekout.text.toString(),
                v.et_lama.text.toString())
            v.et_reservasi.setText("")
            v.sp_penyewa.setSelection(0)
            v.sp_jenis.setSelection(0)
            v.et_cekin.setText("")
            v.et_cekout.setText("")
            v.et_lama.setText("")
        }
    }

    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataNilai(v.et_reservasi.text.toString())
        v.et_reservasi.setText("")
        v.sp_penyewa.setSelection(0)
        v.sp_jenis.setSelection(0)
        v.et_cekin.setText("")
        v.et_cekout.setText("")
        v.et_lama.setText("")
    }

    override fun onClick(v: View?) {
        when(v?.id) {
            R.id.delete_res -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Yakin akan menghapus data ini?")
                    .setPositiveButton("Ya", btnDeleteDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.add_res -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.edit_res -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan diubah sudah benar?")
                    .setPositiveButton("Ya", btnUpdateDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }


}
