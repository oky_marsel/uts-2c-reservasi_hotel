package marsel.oky.uts2creservasihotel

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_penyewa.*
import kotlinx.android.synthetic.main.fragment_penyewa.view.*
import kotlinx.android.synthetic.main.fragment_penyewa.view.sp_kelamin

class PenyewaFragment : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaJk : String=""
    var arrJk = ArrayList<String>()
    lateinit var db : SQLiteDatabase
    var namaPenyewa :String=""

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        v.et_nik.setText(c.getString(c.getColumnIndex("_id")))
        v.et_nama.setText(c.getString(c.getColumnIndex("nama")))
        namaJk = c.getString(c.getColumnIndex("nama_jk"))
        v.sp_kelamin.setSelection(getIndex(v.sp_kelamin,namaJk,arrJk))
        v.et_lahir.setText(c.getString(c.getColumnIndex("lahir")))
        v.tv_tglahir.setText(c.getString(c.getColumnIndex("tglahir")))
        v.et_nohp.setText(c.getString(c.getColumnIndex("nohp")))
        v.et_alamat.setText(c.getString(c.getColumnIndex("alamat")))
    }

    fun getIndex(spinner: Spinner, myString: String, arrayL: ArrayList<String>): Int {
        var a = spinner.count
        Log.d("Output","Jumlah item dalam array = $a")
        var b : String = ""
        for (i in 0 until a) {
            b = arrayL.get(i)
            Log.d("Output","Index ke $i = $b")
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun showDataKelamin(){
        val c : Cursor = db.rawQuery("select nama_jk as _id from jk order by nama_jk asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1), CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.sp_kelamin.adapter = spAdapter
        v.sp_kelamin.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrJk.add(temp) //add the item
            c.moveToNext()
        }
    }

    fun showDataPenyewa(){
        var sql =""
        if(!namaPenyewa.trim().equals("")){
            sql = "select m.nik as _id, m.nama, p.nama_jk, m.lahir, m.tglahir, m.nohp, m.alamat from penyewa m, jk p " +
                    "where m.id_jk = p.id_jk and m.nama like '%$namaPenyewa%'"
        } else{
            sql = "select m.nik as _id, m.nama, p.nama_jk, m.lahir, m.tglahir, m.nohp, m.alamat from penyewa m, jk p " +
                    "where m.id_jk = p.id_jk order by m.nama asc"
        }
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_penyewa,c,
            arrayOf("_id","nama","nama_jk","lahir","tglahir","nohp","alamat"), intArrayOf(R.id.tx_nik,R.id.tx_nama, R.id.tx_kelamin,R.id.tx_lahir,R.id.tx_tglahir,R.id.tx_nohp,R.id.tx_alamat),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lv_penyewa.adapter = lsAdapter
    }

    fun insertDataPenyewa(nik : String , nama: String, id_jk : Int, lahir:String, tglahir:String, nohp:String, alamat:String){
        var sql = "insert into penyewa (nik,nama,id_jk,lahir,tglahir,nohp,alamat) values (?,?,?,?,?,?,?)"
        db.execSQL(sql, arrayOf(nik,nama,id_jk,lahir,tglahir,nohp,alamat))
        showDataPenyewa()
    }
    val btnInsertDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_jk from jk where nama_jk ='$namaJk'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            insertDataPenyewa(v.et_nik.text.toString(), v.et_nama.text.toString(),
                c.getInt(c.getColumnIndex("id_jk")),v.et_lahir.text.toString(),
                v.tv_tglahir.text.toString(),v.et_nohp.text.toString(),v.et_alamat.text.toString())
            v.et_nik.setText("")
            v.et_nama.setText("")
            v.sp_kelamin.setSelection(0)
            v.et_lahir.setText("")
            v.tv_tglahir.setText("")
            v.et_nohp.setText("")
            v.et_alamat.setText("")
        }
    }
    fun updatePenyewa(nik : String , nama: String, id_jk : Int, lahir:String, tglahir:String, nohp:String, alamat:String){
        var cv : ContentValues = ContentValues()
        cv.put("nama",nama)
        cv.put("id_jk",id_jk)
        cv.put("lahir",lahir)
        cv.put("tglahir",tglahir)
        cv.put("nohp",nohp)
        cv.put("alamat",alamat)
        db.update("penyewa",cv,"nik = '$nik'",null)
        showDataPenyewa()
    }
    val btnUpdateDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_jk from jk where nama_jk ='$namaJk'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            updatePenyewa(v.et_nik.text.toString(), v.et_nama.text.toString(),
                c.getInt(c.getColumnIndex("id_jk")),v.et_lahir.text.toString(),
                v.tv_tglahir.text.toString(),v.et_nohp.text.toString(),v.et_alamat.text.toString())
            v.et_nik.setText("")
            v.et_nama.setText("")
            v.sp_kelamin.setSelection(0)
            v.et_lahir.setText("")
            v.tv_tglahir.setText("")
            v.et_nohp.setText("")
            v.et_alamat.setText("")
        }
    }
    fun deleteDataMatkul(nik: String){
        db.delete("penyewa","nik = '$nik'",null)
        showDataPenyewa()
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataMatkul(v.et_nik.text.toString())
        v.et_nik.setText("")
        v.et_nama.setText("")
        v.sp_kelamin.setSelection(0)
        v.et_lahir.setText("")
        v.tv_tglahir.setText("")
        v.et_nohp.setText("")
        v.et_alamat.setText("")
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_penyewa,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.delete_penyewa.setOnClickListener(this)
        v.add_penyewa.setOnClickListener(this)
        v.edit_penyewa.setOnClickListener(this)
        v.sp_kelamin.onItemSelectedListener = this
        v.lv_penyewa.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataKelamin()
        showDataPenyewa()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.delete_penyewa->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dihapus sudah benar?")
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.edit_penyewa->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan diubah sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.add_penyewa ->{
                dialog.setTitle("Konfirmasi").setMessage("Apakah Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()

            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        sp_kelamin.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaJk = c.getString(c.getColumnIndex("_id"))
    }


}
