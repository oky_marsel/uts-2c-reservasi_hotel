package marsel.oky.uts2creservasihotel
import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_kamar.*
import kotlinx.android.synthetic.main.fragment_kamar.view.*
import kotlinx.android.synthetic.main.fragment_kamar.view.spJns


class KamarFragment : Fragment(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
        spJns.setSelection(0, true)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val c : Cursor = spAdapter.getItem(position) as Cursor
        namaJns = c.getString(c.getColumnIndex("_id"))
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btn_Del->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnDeleteDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btn_Upd->{
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang akan dimasukkan sudah benar?")
                    .setPositiveButton("Ya",btnUpdateDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()
            }
            R.id.btn_Ins ->{
                dialog.setTitle("Konfirmasi").setMessage("Data yang akan dimasukkan sudah benar?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya",btnInsertDialog)
                    .setNegativeButton("Tidak",null)
                dialog.show()

            }
        }
    }

    lateinit var thisParent: MainActivity
    lateinit var lsAdapter : ListAdapter
    lateinit var spAdapter: SimpleCursorAdapter
    lateinit var dialog: AlertDialog.Builder
    lateinit var v : View
    var namaJns : String=""
    var arrJns = ArrayList<String>()
    lateinit var db : SQLiteDatabase

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_kamar,container,false)
        db = thisParent.getDbObject()
        dialog = AlertDialog.Builder(thisParent)
        v.btn_Ins.setOnClickListener(this)
        v.btn_Del.setOnClickListener(this)
        v.btn_Upd.setOnClickListener(this)
        v.spJns.onItemSelectedListener = this
        v.lv_kamar.setOnItemClickListener(itemClick)
        return v
    }

    override fun onStart() {
        super.onStart()
        showDataJenis()
        showDataKamar()
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val c: Cursor = parent.adapter.getItem(position) as Cursor
        v.edNmrKmr.setText(c.getString(c.getColumnIndex("_id")))
        namaJns = c.getString(c.getColumnIndex("nama_jns"))
        v.spJns.setSelection(getIndex(v.spJns,namaJns,arrJns))
        v.edHrg.setText(c.getString(c.getColumnIndex("harga")))
    }

    fun getIndex(
        spinner: Spinner,
        myString: String,
        arrJns: ArrayList<String>
    ): Int {
        var a = spinner.count
        var b : String = ""
        for (i in 0 until a) {
            b = this.arrJns.get(i)
            if (b.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }

    fun showDataKamar(){
        var sql = "select k.nokmr as _id, j.nama_jns, k.harga from kamar k, jenis j " +
                "where k.id_jns = j.id_jns order by k.harga asc"
        val c : Cursor = db.rawQuery(sql,null)
        lsAdapter = SimpleCursorAdapter(thisParent,R.layout.item_kamar,c,
            arrayOf("_id","nama_jns","harga"), intArrayOf(R.id.tx_noKmr,R.id.tx_jenis, R.id.tx_Harga),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        v.lv_kamar.adapter = lsAdapter
    }

    fun showDataJenis(){
        val c : Cursor = db.rawQuery("select nama_jns as _id from jenis order by nama_jns asc", null)
        spAdapter = SimpleCursorAdapter(thisParent, android.R.layout.simple_spinner_item,c,
            arrayOf("_id"), intArrayOf(android.R.id.text1),CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        v.spJns.adapter = spAdapter
        v.spJns.setSelection(0)
        c.moveToFirst()
        var temp : String = ""
        while (!c.isAfterLast()) {
            temp = c.getString(c.getColumnIndex("_id"))
            arrJns.add(temp) //add the item
            c.moveToNext()
        }
    }
    fun insertDataKamar(nokmr : String , id_jns : Int, harga: String){
        var sql = "insert into kamar (nokmr,id_jns,harga) values (?,?,?)"
        db.execSQL(sql, arrayOf(nokmr,id_jns,harga))
        showDataKamar()
    }
    val btnInsertDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_jns from jenis where nama_jns ='$namaJns'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            insertDataKamar(v.edNmrKmr.text.toString(),
                c.getInt(c.getColumnIndex("id_jns")), v.edHrg.text.toString())
            v.edNmrKmr.setText("")
            v.spJns.setSelection(0)
            v.edHrg.setText("")
        }
    }

    fun updateDataKamar(nokmr : String , id_jns : Int, harga: String){
        var cv : ContentValues = ContentValues()
        cv.put("id_jns",id_jns)
        cv.put("harga",harga)
        db.update("kamar",cv,"nokmr = '$nokmr'",null)
        showDataKamar()
    }
    val btnUpdateDialog = DialogInterface.OnClickListener{ dialog, which ->
        var sql = "select id_jns from jenis where nama_jns ='$namaJns'"
        val c : Cursor = db.rawQuery(sql, null)
        if(c.count>0){
            c.moveToFirst()
            updateDataKamar(v.edNmrKmr.text.toString(),
                c.getInt(c.getColumnIndex("id_jns")), v.edHrg.text.toString())
            v.edNmrKmr.setText("")
            v.spJns.setSelection(0)
            v.edHrg.setText("")
        }
    }
    fun deleteDataKamar(nokmr: String){
        db.delete("kamar","nokmr = '$nokmr'",null)
        showDataKamar()
    }
    val btnDeleteDialog = DialogInterface.OnClickListener { dialog, which ->
        deleteDataKamar(v.edNmrKmr.text.toString())
        v.edNmrKmr.setText("")
        v.spJns.setSelection(0)
        v.edHrg.setText("")
    }



}


