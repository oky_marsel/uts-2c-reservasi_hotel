package marsel.oky.uts2creservasihotel

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context: Context): SQLiteOpenHelper(context, DB_Name, null, DB_Ver) {
    companion object{
        val DB_Name = "hotel"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tPenyewa = "create table penyewa(id_penyewa integer primary key autoincrement, nik text unique not null, nama text not null, id_jk int not null, lahir text not null, tglahir text not null, nohp text not null, alamat text not null)"
        val tJk ="create table jk(id_jk integer primary key autoincrement, nama_jk text not null) "
        val insJk = "insert into jk(nama_jk) values ('L'),('P')"
        val tKamar = "create table kamar(nokmr text primary key, id_jns integer not null, harga text not null)"
        val tJns = "create table jenis(id_jns integer primary key autoincrement, nama_jns text not null)"
        val insJns = "insert into jenis(nama_jns) values ('Standar Room'),('Superior Room'),('Deluxe Room'),('Junior Suite Room'),('Suite Room'),('Presidental suite Room')"
        val tRes = "create table reservasi(nores text primary key, id_penyewa integer not null, id_jns integer not null, cekin text not null, cekout text not null, lama text not null)"
//        val insKmr = "insert into kamar(nokmr,id_jns,harga) values ('01',2,'5')"
        db?.execSQL(tPenyewa)
        db?.execSQL(tJk)
        db?.execSQL(insJk)
        db?.execSQL(tKamar)
        db?.execSQL(tJns)
        db?.execSQL(insJns)
        db?.execSQL(tRes)
//        db?.execSQL(insKmr)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS penyewa")
        onCreate(db)
        db?.execSQL("DROP TABLE IF EXISTS jk")
        onCreate(db)
        db?.execSQL("DROP TABLE IF EXISTS kamar")
        onCreate(db)
        db?.execSQL("DROP TABLE IF EXISTS jenis")
        onCreate(db)
        db?.execSQL("DROP TABLE IF EXISTS reservasi")
        onCreate(db)
    }
}